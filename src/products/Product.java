package products;

public class Product {
  //Поля продукта
  private String name;
  private String view;
  private String prise;
  private String nalichie;
  private String proizvod;
  private String dateOfVip;
  private String SrokG;
  
  //Конструктор
  public Product(String name, String view, String prise, String nalichie, String proizvod,
      String dateOfVip, String srokG) {
    super();
    this.name = name;
    this.view = view;
    this.prise = prise;
    this.nalichie = nalichie;
    this.proizvod = proizvod;
    this.dateOfVip = dateOfVip;
    SrokG = srokG;
  }
  //Гетерры и сетерры
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getView() {
    return view;
  }

  public void setView(String view) {
    this.view = view;
  }

  public String getPrise() {
    return prise;
  }

  public void setPrise(String prise) {
    this.prise = prise;
  }

  public String getNalichie() {
    return nalichie;
  }

  public void setNalichie(String nalichie) {
    this.nalichie = nalichie;
  }

  public String getProizvod() {
    return proizvod;
  }

  public void setProizvod(String proizvod) {
    this.proizvod = proizvod;
  }

  public String getDateOfVip() {
    return dateOfVip;
  }

  public void setDateOfVip(String dateOfVip) {
    this.dateOfVip = dateOfVip;
  }

  public String getSrokG() {
    return SrokG;
  }

  public void setSrokG(String srokG) {
    SrokG = srokG;
  }
  
  //Переопределение методов для сравнения продуктов.
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((SrokG == null) ? 0 : SrokG.hashCode());
    result = prime * result + ((dateOfVip == null) ? 0 : dateOfVip.hashCode());
    result = prime * result + ((nalichie == null) ? 0 : nalichie.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((prise == null) ? 0 : prise.hashCode());
    result = prime * result + ((proizvod == null) ? 0 : proizvod.hashCode());
    result = prime * result + ((view == null) ? 0 : view.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Product other = (Product) obj;
    if (SrokG == null) {
      if (other.SrokG != null)
        return false;
    } else if (!SrokG.equals(other.SrokG))
      return false;
    if (dateOfVip == null) {
      if (other.dateOfVip != null)
        return false;
    } else if (!dateOfVip.equals(other.dateOfVip))
      return false;
    if (nalichie == null) {
      if (other.nalichie != null)
        return false;
    } else if (!nalichie.equals(other.nalichie))
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    if (prise == null) {
      if (other.prise != null)
        return false;
    } else if (!prise.equals(other.prise))
      return false;
    if (proizvod == null) {
      if (other.proizvod != null)
        return false;
    } else if (!proizvod.equals(other.proizvod))
      return false;
    if (view == null) {
      if (other.view != null)
        return false;
    } else if (!view.equals(other.view))
      return false;
    return true;
  }

}
