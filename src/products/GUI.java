package products;

import java.util.*;

public class GUI extends javax.swing.JFrame {

  private static final long serialVersionUID = 1L;

  //Коллекция для хранения продуктов.
  private HashMap<Integer, Product> table = new HashMap<Integer, Product>();

  private int t = 0;
  //Элементы JFrame
  private javax.swing.JButton Paint;

  private javax.swing.JButton Add;

  private javax.swing.JButton Change;

  private javax.swing.JButton Find;

  private javax.swing.JButton Remove;

  private javax.swing.JTextField TF1;

  private javax.swing.JTextField TF2;

  private javax.swing.JTextField TF3;

  private javax.swing.JTextField TF4;

  private javax.swing.JTextField TF5;

  private javax.swing.JTextField TF6;

  private javax.swing.JTextField TF7;

  private javax.swing.JLabel jLabel1;

  private javax.swing.JLabel jLabel2;

  private javax.swing.JLabel jLabel3;

  private javax.swing.JLabel jLabel4;

  private javax.swing.JLabel jLabel5;

  private javax.swing.JLabel jLabel6;

  private javax.swing.JLabel jLabel7;

  private javax.swing.JLabel jLabel8;

  private javax.swing.JScrollPane jScrollPane1;

  private javax.swing.JTable jTable;

  //Конструктор
  public GUI() {

    initComponents();

  }

  private void initComponents() { //Отрисовка нашего JFrame и всех его частей

    jScrollPane1 = new javax.swing.JScrollPane();

    jTable = new javax.swing.JTable();

    Paint = new javax.swing.JButton();

    Add = new javax.swing.JButton();

    Remove = new javax.swing.JButton();

    Change = new javax.swing.JButton();

    Find = new javax.swing.JButton();

    jLabel1 = new javax.swing.JLabel();

    TF1 = new javax.swing.JTextField();

    TF2 = new javax.swing.JTextField();

    TF3 = new javax.swing.JTextField();

    TF4 = new javax.swing.JTextField();

    TF5 = new javax.swing.JTextField();

    TF6 = new javax.swing.JTextField();

    TF7 = new javax.swing.JTextField();

    jLabel2 = new javax.swing.JLabel();

    jLabel3 = new javax.swing.JLabel();

    jLabel4 = new javax.swing.JLabel();

    jLabel5 = new javax.swing.JLabel();

    jLabel6 = new javax.swing.JLabel();

    jLabel7 = new javax.swing.JLabel();

    jLabel8 = new javax.swing.JLabel();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    
  //Создание модели для нашей таблицы

    jTable.setModel(new javax.swing.table.DefaultTableModel(

        new Object[][] {

            {"", "", "", "", "", "", ""},

            {"", "", "", "", "", "", ""},

            {"", "", "", "", "", "", ""},

            {"", "", "", "", "", "", ""},

            {"", "", "", "", "", "", ""},

        },

        new String[] {

            "Имя товара", "Вид", "Цена", "Наличие на складе", "Производитель", "Дата выпуска",
            "Срок годности"

        }

    ));

    jScrollPane1.setViewportView(jTable);

    Add.setForeground(new java.awt.Color(0, 0, 225));

    Add.setText("Добавить");

    Add.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        AddActionPerformed(evt);

      }

    });

    Paint.setForeground(new java.awt.Color(0, 0, 225));

    Paint.setText("Нарисовать");

    Paint.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        AddActionPerformed(evt);

      }

    });

    Remove.setForeground(new java.awt.Color(0, 0, 225));

    Remove.setText("Удалить");

    Remove.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        RemoveActionPerformed(evt);

      }

    });

    Change.setForeground(new java.awt.Color(0, 0, 225));

    Change.setText("Редактировать");

    Change.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        ChangeActionPerformed(evt);

      }

    });

    Find.setForeground(new java.awt.Color(0, 0, 225));

    Find.setText("Поиск");

    Find.setToolTipText("");

    Find.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        FindActionPerformed(evt);

      }

    });

    jLabel1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

    jLabel1.setForeground(new java.awt.Color(255, 0, 0));

    jLabel1.setText("Введите данные");

    TF1.addFocusListener(new java.awt.event.FocusAdapter() { //Добавление активности при нажатии

      public void focusGained(java.awt.event.FocusEvent evt) {

        TF1FocusGained(evt);

      }

    });

    TF1.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        TF1ActionPerformed(evt);

      }

    });

    TF2.addFocusListener(new java.awt.event.FocusAdapter() {//Добавление активности при нажатии

      public void focusGained(java.awt.event.FocusEvent evt) {

        TF2FocusGained(evt);

      }

    });

    TF2.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        TF2ActionPerformed(evt);

      }

    });

    TF3.addFocusListener(new java.awt.event.FocusAdapter() {//Добавление активности при нажатии

      public void focusGained(java.awt.event.FocusEvent evt) {

        TF3FocusGained(evt);

      }

    });

    TF3.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        TF3ActionPerformed(evt);

      }

    });

    TF4.addFocusListener(new java.awt.event.FocusAdapter() {//Добавление активности при нажатии

      public void focusGained(java.awt.event.FocusEvent evt) {

        TF4FocusGained(evt);

      }

    });

    TF4.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        TF4ActionPerformed(evt);

      }

    });

    TF5.addFocusListener(new java.awt.event.FocusAdapter() {//Добавление активности при нажатии

      public void focusGained(java.awt.event.FocusEvent evt) {

        TF5FocusGained(evt);

      }

    });

    TF5.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        TF5ActionPerformed(evt);

      }

    });

    TF6.addFocusListener(new java.awt.event.FocusAdapter() {//Добавление активности при нажатии

      public void focusGained(java.awt.event.FocusEvent evt) {

        TF6FocusGained(evt);

      }

    });

    TF6.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        TF6ActionPerformed(evt);

      }

    });

    TF7.addFocusListener(new java.awt.event.FocusAdapter() {//Добавление активности при нажатии

      public void focusGained(java.awt.event.FocusEvent evt) {

        TF7FocusGained(evt);

      }

    });

    TF7.addActionListener(new java.awt.event.ActionListener() {//Добавление активности при нажатии

      public void actionPerformed(java.awt.event.ActionEvent evt) {

        TF7ActionPerformed(evt);

      }

    });

    jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

    jLabel2.setForeground(new java.awt.Color(90, 0, 157));

    jLabel2.setText("Имя");

    jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

    jLabel3.setForeground(new java.awt.Color(90, 0, 157));

    jLabel3.setText("Вид");

    jLabel3.setToolTipText("");

    jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

    jLabel3.setForeground(new java.awt.Color(90, 0, 157));

    jLabel3.setText("Цена");

    jLabel3.setToolTipText("");

    jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

    jLabel4.setForeground(new java.awt.Color(90, 0, 157));

    jLabel4.setText("Наличие на скл.");

    jLabel4.setToolTipText("");

    jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

    jLabel5.setForeground(new java.awt.Color(90, 0, 157));

    jLabel5.setText("Производитель");

    jLabel5.setToolTipText("");

    jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

    jLabel6.setForeground(new java.awt.Color(90, 0, 157));

    jLabel6.setText("Дата выпуска");

    jLabel6.setToolTipText("");

    jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

    jLabel7.setForeground(new java.awt.Color(90, 0, 157));

    jLabel7.setText("Срок годности");

    jLabel7.setToolTipText("");

    jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

    jLabel8.setForeground(new java.awt.Color(90, 0, 157));

    jLabel8.setText("Вид");

    jLabel8.setToolTipText("");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());

    getContentPane().setLayout(layout);
    //Задание X координаты для каждого из элементов
    layout.setHorizontalGroup(

        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(layout.createSequentialGroup()

                .addGap(28, 28, 28)

                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                    .addGroup(layout.createSequentialGroup()

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                            javax.swing.GroupLayout.DEFAULT_SIZE, 80)
                        .addComponent(Paint)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                            javax.swing.GroupLayout.DEFAULT_SIZE, 40)
                        .addComponent(Add)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                            javax.swing.GroupLayout.DEFAULT_SIZE, 100)

                        .addGroup(
                            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)

                                .addComponent(Remove)

                                .addComponent(Find))

                        .addGap(37, 37, 37)

                        .addComponent(Change)

                        .addGap(25, 25, 25))

                    .addGroup(layout.createSequentialGroup()

                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 730,
                            javax.swing.GroupLayout.PREFERRED_SIZE)

                        .addContainerGap(53, Short.MAX_VALUE))

                    .addGroup(layout.createSequentialGroup()

                        .addComponent(TF1, javax.swing.GroupLayout.PREFERRED_SIZE, 100,
                            javax.swing.GroupLayout.PREFERRED_SIZE)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                            javax.swing.GroupLayout.DEFAULT_SIZE, 5)

                        .addComponent(TF2, javax.swing.GroupLayout.PREFERRED_SIZE, 100,
                            javax.swing.GroupLayout.PREFERRED_SIZE)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                            javax.swing.GroupLayout.DEFAULT_SIZE, 5)

                        .addComponent(TF3, javax.swing.GroupLayout.PREFERRED_SIZE, 100,
                            javax.swing.GroupLayout.PREFERRED_SIZE)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                            javax.swing.GroupLayout.DEFAULT_SIZE, 5)

                        .addComponent(TF4, javax.swing.GroupLayout.PREFERRED_SIZE, 100,
                            javax.swing.GroupLayout.PREFERRED_SIZE)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                            javax.swing.GroupLayout.DEFAULT_SIZE, 5)

                        .addComponent(TF5, javax.swing.GroupLayout.PREFERRED_SIZE, 100,
                            javax.swing.GroupLayout.PREFERRED_SIZE)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                            javax.swing.GroupLayout.DEFAULT_SIZE, 5)

                        .addComponent(TF6, javax.swing.GroupLayout.PREFERRED_SIZE, 100,
                            javax.swing.GroupLayout.PREFERRED_SIZE)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                            javax.swing.GroupLayout.DEFAULT_SIZE, 5)

                        .addComponent(TF7, javax.swing.GroupLayout.PREFERRED_SIZE, 100,
                            javax.swing.GroupLayout.PREFERRED_SIZE)

                        .addGap(62, 62, 62))))

            .addGroup(layout.createSequentialGroup()

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                    javax.swing.GroupLayout.DEFAULT_SIZE, 230)

                .addGap(110, 110, 110)

                .addComponent(jLabel1)

                .addGap(0, 0, 100))

            .addGroup(layout.createSequentialGroup()

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                    javax.swing.GroupLayout.DEFAULT_SIZE, 60)

                .addComponent(jLabel2)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                    javax.swing.GroupLayout.DEFAULT_SIZE, 80)

                .addComponent(jLabel8)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                    javax.swing.GroupLayout.DEFAULT_SIZE, 80)

                .addComponent(jLabel3)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                    javax.swing.GroupLayout.DEFAULT_SIZE, 40)

                .addComponent(jLabel4)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                    javax.swing.GroupLayout.DEFAULT_SIZE, 15)

                .addComponent(jLabel5)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                    javax.swing.GroupLayout.DEFAULT_SIZE, 15)

                .addComponent(jLabel6)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                    javax.swing.GroupLayout.DEFAULT_SIZE, 15)

                .addComponent(jLabel7)

            )

    );
    //Задание Y координаты для каждого из элементов
    layout.setVerticalGroup(

        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()

                .addGap(26, 26, 26)

                .addComponent(Find)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)

                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)

                    .addComponent(Paint)

                    .addComponent(Remove)

                    .addComponent(Change)

                    .addComponent(Add))

                .addGap(36, 36, 36)

                .addComponent(jLabel1)

                .addGap(13, 13, 13)

                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)

                    .addComponent(jLabel2)

                    .addComponent(jLabel3)

                    .addComponent(jLabel4)

                    .addComponent(jLabel5)

                    .addComponent(jLabel6)

                    .addComponent(jLabel7)

                    .addComponent(jLabel8)

                )

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                .addGroup(layout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)

                    .addComponent(TF1, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)

                    .addComponent(TF2).addComponent(TF3).addComponent(TF4).addComponent(TF5)
                    .addComponent(TF6).addComponent(TF7))

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                    javax.swing.GroupLayout.DEFAULT_SIZE, 50)

                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 111,
                    javax.swing.GroupLayout.PREFERRED_SIZE)

                .addContainerGap())

    );

    setBounds(0, 0, 800, 450);

  }

  private void AddActionPerformed(java.awt.event.ActionEvent evt) { //Метод для добавления продукта

    String name, view, prise, nalichie, proizvod, dateOfVip, SrokG;

    name = TF1.getText();

    view = TF2.getText();

    prise = TF3.getText();

    nalichie = TF4.getText();

    proizvod = TF5.getText();

    dateOfVip = TF6.getText();

    SrokG = TF7.getText();

    if (!view.isEmpty() && !name.isEmpty() && !prise.isEmpty() && !nalichie.isEmpty()
        && !proizvod.isEmpty() && !dateOfVip.isEmpty() && !SrokG.isEmpty()) {
      Product p = new Product(name, view, prise, nalichie, proizvod, dateOfVip, SrokG);
      table.put(t, p);
      jLabel1.setText("Продукт добавлен");
      jTable.setValueAt(name, t, 0);
      jTable.setValueAt(view, t, 1);
      jTable.setValueAt(prise, t, 2);
      jTable.setValueAt(nalichie, t, 3);
      jTable.setValueAt(proizvod, t, 4);
      jTable.setValueAt(dateOfVip, t, 5);
      jTable.setValueAt(SrokG, t, 6);
      t++;
    } else {
      jLabel1.setText("Вы ввели некорректные данные");
    }

  }

  private void FindActionPerformed(java.awt.event.ActionEvent evt) {//Метод для поиска продукта

    String name;

    name = TF1.getText();

    if (!name.isEmpty()) {
      Iterator<Map.Entry<Integer, Product>> entries = table.entrySet().iterator();
      while (entries.hasNext()) {
        Map.Entry<Integer, Product> entry = entries.next();
        Product findProduct = entry.getValue();
        if (findProduct.getName().equals(name)) {
          jLabel1.setText("Продукт найден");
          t = 0;
          jTable.setValueAt(findProduct.getName(), t, 0);
          jTable.setValueAt(findProduct.getView(), t, 1);
          jTable.setValueAt(findProduct.getPrise(), t, 2);
          jTable.setValueAt(findProduct.getNalichie(), t, 3);
          jTable.setValueAt(findProduct.getProizvod(), t, 4);
          jTable.setValueAt(findProduct.getDateOfVip(), t, 5);
          jTable.setValueAt(findProduct.getSrokG(), t, 6);
          t++;
          while (t < 5) {
            for (int i = 0; i < 7; i++) {
              jTable.setValueAt("", t, i);
            }
            t++;
          }
          break;
        }
      }

    } else {
      jLabel1.setText("Продукт с таким именем не найден");
    }

  }

  private void ChangeActionPerformed(java.awt.event.ActionEvent evt) {//Метод для редактирования продукта

    String name, view, prise, nalichie, proizvod, dateOfVip, SrokG;

    name = TF1.getText();

    view = TF2.getText();

    prise = TF3.getText();

    nalichie = TF4.getText();

    proizvod = TF5.getText();

    dateOfVip = TF6.getText();

    SrokG = TF7.getText();

    if (!view.isEmpty() && !name.isEmpty() && !prise.isEmpty() && !nalichie.isEmpty()
        && !proizvod.isEmpty() && !dateOfVip.isEmpty() && !SrokG.isEmpty()) {
      Product p = new Product(name, view, prise, nalichie, proizvod, dateOfVip, SrokG);
      Iterator<Map.Entry<Integer, Product>> entries = table.entrySet().iterator();
      while (entries.hasNext()) {
        Map.Entry<Integer, Product> entry = entries.next();
        Product findProduct = entry.getValue();
        if (findProduct.getName().equals(name)) {
          int count = entry.getKey();
          jTable.clearSelection();
          jLabel1.setText("Продукт обновлен");
          jTable.setValueAt(findProduct.getName(), count, 0);
          jTable.setValueAt(view, count, 1);
          jTable.setValueAt(prise, count, 2);
          jTable.setValueAt(nalichie, count, 3);
          jTable.setValueAt(proizvod, count, 4);
          jTable.setValueAt(dateOfVip, count, 5);
          jTable.setValueAt(SrokG, count, 6);
          table.put(count, p);
        }
      }
      paintTable();
    } else {
      jLabel1.setText("Продукт с таким именем не найден");
    }

  }

  private void RemoveActionPerformed(java.awt.event.ActionEvent evt) {//Метод для удаления продукта

    String name;

    name = TF1.getText();

    if (!name.isEmpty()) {
      Iterator<Map.Entry<Integer, Product>> entries = table.entrySet().iterator();
      while (entries.hasNext()) {
        Map.Entry<Integer, Product> entry = entries.next();
        Product findProduct = entry.getValue();
        if (findProduct.getName().equals(name)) {
          entries.remove();
        }
      }
      paintTable();
    } else {
      jLabel1.setText("Продукт с таким именем не найден");
    }

  }

  private void TF1ActionPerformed(java.awt.event.ActionEvent evt) {

    TF1.setText("");

  }

  private void TF2ActionPerformed(java.awt.event.ActionEvent evt) {

    TF2.setText("");

  }

  private void TF3ActionPerformed(java.awt.event.ActionEvent evt) {

    TF3.setText("");

  }

  private void TF4ActionPerformed(java.awt.event.ActionEvent evt) {

    TF4.setText("");

  }

  private void TF5ActionPerformed(java.awt.event.ActionEvent evt) {

    TF5.setText("");

  }

  private void TF6ActionPerformed(java.awt.event.ActionEvent evt) {

    TF6.setText("");

  }

  private void TF7ActionPerformed(java.awt.event.ActionEvent evt) {

    TF7.setText("");

  }

  private void TF1FocusGained(java.awt.event.FocusEvent evt) {

    TF1.setText("");

  }

  private void TF2FocusGained(java.awt.event.FocusEvent evt) {

    TF2.setText("");

  }

  private void TF3FocusGained(java.awt.event.FocusEvent evt) {

    TF3.setText("");

  }

  private void TF4FocusGained(java.awt.event.FocusEvent evt) {

    TF4.setText("");

  }

  private void TF5FocusGained(java.awt.event.FocusEvent evt) {

    TF5.setText("");

  }

  private void TF6FocusGained(java.awt.event.FocusEvent evt) {

    TF6.setText("");

  }

  private void TF7FocusGained(java.awt.event.FocusEvent evt) {

    TF7.setText("");

  }

  public static void main(String[] args) {
    GUI Start = new GUI();
    Start.setVisible(true);
  }

  private void paintTable() {//Метод для заполнения нашей таблицы 
    Iterator<Map.Entry<Integer, Product>> entries = table.entrySet().iterator();
    t = 0;
    while (t < 5) {
      for (int i = 0; i < 7; i++) {
        jTable.setValueAt("", t, i);
      }
      t++;
    }
    while (entries.hasNext()) {
      Map.Entry<Integer, Product> entry = entries.next();
      Product findProduct = entry.getValue();
      jTable.clearSelection();
      jLabel1.setText("Таблица нарисована");
      jTable.setValueAt(findProduct.getName(), t, 0);
      jTable.setValueAt(findProduct.getView(), t, 1);
      jTable.setValueAt(findProduct.getPrise(), t, 2);
      jTable.setValueAt(findProduct.getNalichie(), t, 3);
      jTable.setValueAt(findProduct.getProizvod(), t, 4);
      jTable.setValueAt(findProduct.getDateOfVip(), t, 5);
      jTable.setValueAt(findProduct.getSrokG(), t, 6);
      t++;
    }
  }

}
